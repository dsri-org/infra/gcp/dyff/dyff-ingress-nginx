# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

locals {
  cluster_name = "${var.environment}-dyff-cloud"
  deployment   = "dyff-ingress-nginx"
  region       = "us-central1"
  name         = "${var.environment}-${local.deployment}"
}
