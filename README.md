# dyff-ingress-nginx deployment

<!-- BADGIE TIME -->

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![cici-tools enabled](https://img.shields.io/badge/%E2%9A%A1_cici--tools-enabled-c0ff33)](https://gitlab.com/buildgarden/tools/cici-tools)

<!-- END BADGIE TIME -->

## Bootstrap procedure

### Prerequisites

- The following services must be enabled:

  - `cloudresourcemanager.googleapis.com`

  - `container.googleapis.com`

  - `iam.googleapis.com`

### Steps

- Create a service account.

- At org level, grant:

  - `Compute Admin`

  - `Kubernetes Engine Cluster Admin`

  - `Service Account Admin`

- Create a service account API key and save in the project root directory as
  `credentials.json`.

<!-- prettier-ignore-start -->
<!-- prettier-ignore-end -->
