# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "kubernetes_namespace" "ingress_nginx" {
  metadata {
    name = "ingress-nginx"
    labels = {
      # https://kubernetes.io/docs/concepts/security/pod-security-standards/
      "pod-security.kubernetes.io/enforce" = "restricted"
    }
  }
}

# https://artifacthub.io/packages/helm/bitnami/nginx-ingress-controller
resource "helm_release" "ingress_nginx" {
  name       = "ingress-nginx"
  namespace  = kubernetes_namespace.ingress_nginx.metadata.0.name
  repository = "oci://registry-1.docker.io/bitnamicharts"
  chart      = "nginx-ingress-controller"
  version    = "11.0.0"

  skip_crds = true

  wait          = false
  wait_for_jobs = false

  values = [yamlencode({
    autoscaling = {
      enabled     = true
      maxReplicas = 3
      minReplicas = 3
      targetCPU   = 80
    }

    fullnameOverride = "ingress-nginx"

    metrics = {
      enabled = true
    }

    podAntiAffinityPreset = "hard"

    # Needed so that FastAPI url_for respects https under nginx+uvicorn
    # See: https://stackoverflow.com/a/66244482
    proxySetHeaders = {
      X-Forwarded-For   = "$proxy_add_x_forwarded_for"
      X-Forwarded-Proto = "$scheme"
    }

    publishService = {
      # This MUST be enabled, otherwise Ingress resources will publish internal
      # node IPs via external-dns.
      enabled = true
    }

    resourcesPreset = "small"

    topologyKey = "topology.kubernetes.io/zone"
  })]
}

# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/manifest
# https://cloud.google.com/stackdriver/docs/managed-prometheus/setup-managed#gmp-servicemonitor
resource "kubernetes_manifest" "pod_monitoring" {
  manifest = {
    apiVersion = "monitoring.googleapis.com/v1"
    kind       = "PodMonitoring"
    metadata = {
      name      = "ingress-nginx-metrics"
      namespace = kubernetes_namespace.ingress_nginx.metadata[0].name
    }
    spec = {
      selector = {
        matchLabels = {
          "app.kubernetes.io/instance"  = "ingress-nginx"
          "app.kubernetes.io/name"      = "nginx-ingress-controller"
          "app.kubernetes.io/component" = "controller"
        }
      }
      endpoints = [{
        port = "metrics"
        path = "/metrics"
      }],
    }
  }
}
